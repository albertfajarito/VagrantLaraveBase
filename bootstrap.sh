#!/usr/bin/env bash
environment="local"
coredb="ccms"
appurl="ucsr-dev.local"
mysqlhost="127.0.0.1"
mysqluser="root"
mysqlpassword="vagrant"
ucsrrepo="git@bitbucket.org:concentrixcorp/ultimatecsr-app.git"
developbranch="develop"
rootfolder="webroot"
nginxversion="1.12.2"
phpversion="7.1"
nodeversion="8.10.0"
bbmessage="Copy your ssh-key to Bitbucket"

sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install -y python-software-properties

echo -e "\e[31m=====INSTALL GIT=====\e[0m"
sudo apt-get install -y git-core

echo -e "\e[31m=====INSTALLING NGINX=====\e[0m"
sudo apt-get install -y nginx

echo -e "\e[31m=====OVERRIDE DEFAULT NGINX CONF=====\e[0m"
sudo cp /vagrant/app.conf /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/app.conf /etc/nginx/sites-enabled/app.conf
sudo service nginx restart

if ! [ -L /var/www ]; then
    sudo  rm -rf /var/www
	sudo ln -fs /vagrant /var/www
fi

echo -e "\e[31m=====INSTALLING MYSQL=====\e[0m"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $mysqlpassword"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $mysqlpassword"
sudo apt-get install -y mysql-server mysql-client

echo -e "\e[31m=====INSTALLING PHP 7.1 AND MODULES=====\e[0m"
sudo apt-get install -y php7.1-cli php7.1-common php7.1 php7.1-mysql php7.1-fpm php7.1-curl php7.1-mcrypt php7.1-gd php7.1-xml php7.1-zip php7.1-mbstring php-memcached ntpdate

echo -e "\e[31m=====SYNC SERVER TIME=====\e[0m"
sudo ntpdate time.apple.com

echo -e "\e[31m=====CONFIGURE SERVER TIMEZONE DATA=====\e[0m"
sudo dpkg-reconfigure tzdata

echo -e "\e[31m=====INSTALLING COMPOSER=====\e[0m"
cd /vagrant
php -r "readfile('https://getcomposer.org/installer');" | php
sudo mv composer.phar /usr/local/bin/composer
sudo chmod 777 /usr/local/bin/composer

echo -e "\e[31m=====INSTALLING LUMEN INSTALLER=====\e[0m"
composer global require "laravel/lumen-installer"
echo 'export PATH="$PATH:$HOME/.config/.composer/vendor/bin"' >> ~/.bashrc

echo -e "\e[31m=====SETUP SWAP FILE=====\e[0m"
sudo /bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024
sudo /sbin/mkswap /var/swap.1
sudo /sbin/swapon /var/swap.1

echo -e "\e[31m=====INSTALL REDIS=====\e[0m"
sudo apt-get install -y redis-server redis-sentinel redis-tools

echo -e "\e[31m=====START REDIS AS SERVICE=====\e[0m"
sudo service redis-server start

echo -e "\e[31m=====SET REDIS TO AUTOSTART=====\e[0m"
sudo update-rc.d redis-server enable
sudo update-rc.d redis-server defaults

echo -e "\e[31m=====CREATE MYSQL TIMEZONE TABLES=====\e[0m"
mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u $mysqluser -p$mysqlpassword mysql

echo -e "\e[31m=====UPDATE DEVELOPMENT CLIENT DB CREDS=====\e[0m"
mysql -u$mysqluser -p$mysqlpassword $coredb -e "UPDATE ccms.clients SET database_username = '$mysqluser', database_password = '$mysqlpassword', filesystem_root = '/vagrant/webroot/development/client-disks/ultimatecsr'  WHERE id = 1;"

echo -e "\e[31m=====ENABLE REMOTE CONNECTION=====\e[0m"
mysql -u$mysqluser -p$mysqlpassword $coredb -e "UPDATE mysql.user SET Host='%' WHERE Host='localhost' AND User='root';"
mysql -u$mysqluser -p$mysqlpassword $coredb -e "FLUSH PRIVILEGES;"

echo -e "\e[31m=====START NGINX=====\e[0m"
sudo service nginx start

echo -e "\e[31m=====RESTART PHP-FPM=====\e[0m"
sudo service php$phpversion-fpm restart

echo -e "\e[31m=====INSTALL PHP DEPENDENCY=====\e[0m"
cd /vagrant/webroot
composer install

echo -e "\e[31m=====SET SUPERVISOR SERVICE FOR LARAVEL JOBS=====\e[0m"
sudo apt-get install -y supervisor
sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl start all

echo -e "\e[31m=====SET UCSR CRON JOB=====\e[0m"
sudo service cron restart
sudo service cron reload

echo -e "\e[31m=====INSTALL HEADLESS DUSK TESTING=====\e[0m"
wget -qO- https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
sudo apt-get update && sudo apt-get install -y google-chrome-stable
sudo apt-get -y install libxpm4 libxrender1 libgtk2.0-0 liibnss3 libgconf-2-4
sudo apt-get -y install xvfb gtk2-engines-pixbuf
sudo apt-get -y install xfonts-cyrillic xfonts-100dpi xfonts-75dpi xfonts-base xfonts-scalable
sudo apt-get -y install imagemagick x11-apps
chmod a+x /vagrant/webroot/vendor/laravel/dusk/bin/chromedriver-linux
sudo sh -c 'echo "127.0.0.1 ucsr-dev.local" >> /etc/hosts'
sudo apt-get -y install libgconf-2-4
mkdir /vagrant/webroot/tests/Browser/screenshots

echo -e "\e[31m=====VAGRANT PROVISIONING DONE=====\e[0m"
